﻿using System;
using System.Collections.Generic;
using System.Text;

namespace instagram_hw
{
    public class User
    {
        public int ID { get; set; }
      
        public string Login { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime BirthDay { get; set; }
        public DateTime AccountCreationDate { get; set; }

    }
}
