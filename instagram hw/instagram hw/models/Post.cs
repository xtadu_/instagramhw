﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace instagram_hw
{
    public class Post
    {
        public int PostID { get; set; }
       
        public int UserId { get; set; }
        public string Comment { get; set; }
        public virtual User User { get; set; }

    }
}
