﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace instagram_hw
{
    public class AplicationDbContext : DbContext
    {

        public DbSet<User> User { get; set; }
        public DbSet<Post> Post { get; set; }

        public AplicationDbContext()
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(@"C:\GitRepository\hw40\instagram hw\instagram hw\Migration");
            builder.AddJsonFile("Migration.json");
            var config = builder.Build();
            var connectionString = config.GetConnectionString("DefaultConnection");
            optionsBuilder.UseSqlServer(connectionString);
        }
    }
}
